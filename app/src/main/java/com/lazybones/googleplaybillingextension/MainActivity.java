package com.lazybones.googleplaybillingextension;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RunnerActivity.setCurrentActivity(this);

        GooglePlayBillingWrapper.initialize("ljgiuvhbjn");

        //GooglePlayBillingWrapper.consumeAsync("123");
        //GooglePlayBillingWrapper.consumeAsync("456");
    }

    //public void verify(View view) {
    //    boolean verified = GooglePlayBillingWrapper.verifyPurchase(
    //            "{\"orderId\":\"GPA.3331-1205-1638-63872\",\"packageName\":\"com.artemlatotin.battleforhexagon\",\"productId\":\"full_game\",\"purchaseTime\":1555505091197,\"purchaseState\":0,\"purchaseToken\":\"haajmapccgjhpehlknlekbig.AO-J1OyGkmnC04-Jl-2j19yETBepo0sW8iuftJRs8XEk2iwacqE3_o3iNIzt469k4kBuUunfbjn4_LuuZC7uZfAYODV9mhNr7KBJF61x3g5dgwfEV76sLI0tNINDlUn9gNPIWr-Z2w5a\"}",
    //            "DKaGg0p9elNdGr4KERfPctXZ39A5n6dXg9OZ4bpysD42Gkl66ThugADDzwjtiHBppy2vL1371KhpPUKOm73PNDfLPIWPn/z0mFhIXvYcFJJvqQ56r4ju4ckU7xoo9M1VTT4dHvdrtdYNVraiQenWz7JZNGFQE+B57MRSCmu2oX8WPNapY9OLiCSCNd/RJzrQlUNIKSjOLblP0tuKi+9LgrbuqVSal87hynv9kbhy5IGnWMIOUKSR05P2vFUsWnWXT2MzQb2MaLiNxcKATytxtSd4N+YbgMAJxQCl8sMtHQCatSmAYWI4LvV+xu+CdJ4DYn0890FY54dX6bg9mXX61w==");
    //    Log.e("yoyo", "Verified: " + verified);
    //}
}
