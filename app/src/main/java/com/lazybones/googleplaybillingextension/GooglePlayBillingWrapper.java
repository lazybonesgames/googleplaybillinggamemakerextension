//+package ${YYAndroidPackageName};
/*-*/package com.lazybones.googleplaybillingextension;

//+import ${YYAndroidPackageName}.R;
//+import com.yoyogames.runner.RunnerJNILib;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.InAppMessageParams;
import com.android.billingclient.api.InAppMessageResponseListener;
import com.android.billingclient.api.InAppMessageResult;
import com.android.billingclient.api.ProductDetails;
import com.android.billingclient.api.ProductDetailsResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.QueryProductDetailsParams;
import com.android.billingclient.api.QueryPurchasesParams;
import com.google.common.collect.ImmutableList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GooglePlayBillingWrapper {

    private final static int EVENT_OTHER_SOCIAL = 70;

    private final static int ASYNC_RESPONSE_INITIALIZE = 60001;
    private final static int ASYNC_RESPONSE_PURCHASES_LIST = 60002;
    private final static int ASYNC_RESPONSE_PURCHASE_UPDATED = 60003;
    private final static int ASYNC_RESPONSE_SKU_DETAILS = 60004;
    private final static int ASYNC_RESPONSE_CONSUME = 60005;

    private static BillingClient billingClient;
    private static PublicKey publicKey;

    private final static String PREF_UNCONSUMED_PURCHASES = "unconsumed_purchases";
    private static List<String> unconsumedPurchases;

    private static void connectToBillingService(@Nullable final Runnable executeOnSuccess) {
        billingClient = BillingClient.newBuilder(RunnerActivity.CurrentActivity).setListener(new PurchasesUpdatedListenerImpl())
                .enablePendingPurchases()
                .build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                int response = RunnerJNILib.jCreateDsMap(null, null, null);
                RunnerJNILib.DsMapAddDouble(response, "responseCode", billingResult.getResponseCode());
                RunnerJNILib.DsMapAddDouble(response, "id", ASYNC_RESPONSE_INITIALIZE);
                RunnerJNILib.CreateAsynEventWithDSMap(response, EVENT_OTHER_SOCIAL);

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && executeOnSuccess != null)
                    executeOnSuccess.run();

                InAppMessageParams inAppMessageParams = InAppMessageParams.newBuilder()
                                        .addInAppMessageCategoryToShow(InAppMessageParams.InAppMessageCategoryId.TRANSACTIONAL)
                                        .build();
                billingClient.showInAppMessages(RunnerActivity.CurrentActivity, inAppMessageParams,
                        new InAppMessageResponseListener() {
                            @Override
                            public void onInAppMessageResponse(@NonNull InAppMessageResult inAppMessageResult) {
                                if (inAppMessageResult.getResponseCode() == InAppMessageResult.InAppMessageResponseCode.SUBSCRIPTION_STATUS_UPDATED)
                                    queryPurchasesAsync(BillingClient.ProductType.SUBS);
                            }
                        });

                //consume unconsumed purchases
                SharedPreferences prefs = RunnerActivity.CurrentActivity.getSharedPreferences(PREF_UNCONSUMED_PURCHASES, Context.MODE_PRIVATE);
                Set<String> unconsumedPurchasesSet = prefs.getStringSet(PREF_UNCONSUMED_PURCHASES, null);
                if (unconsumedPurchasesSet != null) {
                    unconsumedPurchases = new ArrayList<>(unconsumedPurchasesSet);
                    consumeUnconsumedPurchases();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                if (GooglePlayBillingWrapper.isNetworkAvailable()) { //if network is available - will try to reconnect after a while
                    billingClient.endConnection();

                    final Runnable reconnectRunnable = new Runnable() {
                        @Override
                        public void run() {
                            connectToBillingService(null);
                        }
                    };
                    Handler handler = new Handler(RunnerActivity.CurrentActivity.getMainLooper());
                    handler.postDelayed(reconnectRunnable, 30000);
                }
            }
        });
    }

    public static void initialize(String licenseKey) {
        try {
            byte[] decodedKey = Base64.decode(licenseKey, Base64.DEFAULT);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            publicKey = null;
        }

        connectToBillingService(null);
    }

    private final static Runnable consumeRunnable = new Runnable() {
        @Override
        public void run() {
            if (unconsumedPurchases.size() > 0) {
                billingClient.consumeAsync(buildConsumeParams(unconsumedPurchases.get(0)), new ConsumeResponseListener() {
                    @Override
                    public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String purchaseToken) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            unconsumedPurchases.remove(0);
                            storeUnconsumedPurchases();
                            consumeUnconsumedPurchases();
                        }
                        else {
                            Handler handler = new Handler(RunnerActivity.CurrentActivity.getMainLooper());
                            handler.postDelayed(consumeRunnable, 5000); //will try to consume after delay
                        }
                    }
                });
            }
            else {
                unconsumedPurchases = null;
                SharedPreferences.Editor editor = RunnerActivity.CurrentActivity.getSharedPreferences(PREF_UNCONSUMED_PURCHASES, Context.MODE_PRIVATE).edit();
                editor.remove(PREF_UNCONSUMED_PURCHASES);
                editor.apply();
            }
        }
    };
    private static void consumeUnconsumedPurchases() {
        if (billingClient.isReady())
            consumeRunnable.run();
        else
            connectToBillingService(consumeRunnable);
    }
    private static void storeUnconsumedPurchases() {
        SharedPreferences.Editor editor = RunnerActivity.CurrentActivity.getSharedPreferences(PREF_UNCONSUMED_PURCHASES, Context.MODE_PRIVATE).edit();
        editor.putStringSet(PREF_UNCONSUMED_PURCHASES, new HashSet<>(unconsumedPurchases));
        editor.apply();
    }
    private static void addUnconsumedPurchase(String purchaseToken) {
        if (unconsumedPurchases == null) {
            unconsumedPurchases = new ArrayList<>(1);
            unconsumedPurchases.add(purchaseToken);
            consumeUnconsumedPurchases();
        }
        else
            unconsumedPurchases.add(purchaseToken);

        storeUnconsumedPurchases();
    }

    public static void queryPurchasesAsync(final String productType) {
        if (billingClient.isReady()) {
            billingClient.queryPurchasesAsync(QueryPurchasesParams.newBuilder()
                    .setProductType(productType)
                    .build(), new PurchasesResponseListener() {
                        @Override
                        public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> list) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                                JSONArray jSKUsList = new JSONArray();
                                for (Purchase purchase : list) {
                                    if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED && verifyPurchase(purchase))
                                        for (String productId : purchase.getProducts())
                                            jSKUsList.put(productId);
                                }
        
                                int purchasesMap = RunnerJNILib.jCreateDsMap(null, null, null);
                                RunnerJNILib.DsMapAddDouble(purchasesMap, "id", GooglePlayBillingWrapper.ASYNC_RESPONSE_PURCHASES_LIST);
                                RunnerJNILib.DsMapAddDouble(purchasesMap, "responseCode", BillingClient.BillingResponseCode.OK);
                                RunnerJNILib.DsMapAddString(purchasesMap, "skus", jSKUsList.toString());
                                RunnerJNILib.DsMapAddString(purchasesMap, "productType", productType);
                                RunnerJNILib.CreateAsynEventWithDSMap(purchasesMap, EVENT_OTHER_SOCIAL);
                            }
                            else
                                sendEmptyAsyncEvent(ASYNC_RESPONSE_PURCHASES_LIST, billingResult.getResponseCode());
                        }
            });
        }
        else
            sendEmptyAsyncEvent(ASYNC_RESPONSE_PURCHASES_LIST, BillingClient.BillingResponseCode.ERROR);
    }

    public static void consumeAllPurchases() {
        if (billingClient.isReady()) {
            billingClient.queryPurchasesAsync(QueryPurchasesParams.newBuilder()
                    .setProductType(BillingClient.ProductType.INAPP)
                    .build(), new PurchasesResponseListener() {
                @Override
                public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> list) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        for (Purchase purchase : list)
                            consumeAsync(purchase.getPurchaseToken());
                    }
                }
            });
        }
    }

    private static void queryProductDetailsAsync(List<QueryProductDetailsParams.Product> productList, ProductDetailsResponseListener productDetailsResponseListener) {
        QueryProductDetailsParams params = QueryProductDetailsParams.newBuilder()
                .setProductList(productList)
                .build();
        billingClient.queryProductDetailsAsync(params, productDetailsResponseListener);
    }

    public static void queryProductDetailsAsync(String jsonMap, final String productType) {
        try {
            JSONObject jSkus = new JSONObject(jsonMap);
            JSONArray jSkusList = jSkus.getJSONArray("skus");

            ArrayList<QueryProductDetailsParams.Product> productList = new ArrayList<>(jSkusList.length());
            for (int i=0; i<jSkusList.length(); i++) {
                productList.add(QueryProductDetailsParams.Product.newBuilder()
                        .setProductId(jSkusList.getString(i))
                        .setProductType(productType)
                        .build());
            }

            queryProductDetailsAsync(productList, new ProductDetailsResponseListener() {
                @Override
                public void onProductDetailsResponse(@NonNull BillingResult billingResult, @NonNull List<ProductDetails> list) {
                    int responseCode = billingResult.getResponseCode();
                    if (responseCode == BillingClient.BillingResponseCode.OK) {
                        try {
                            JSONArray jSkusDetails = new JSONArray();
                            for (int i = 0; i < list.size(); i++) {
                                ProductDetails productDetails = list.get(i);
                                JSONObject jSkuDetails = new JSONObject();
                                jSkuDetails.put("productId", productDetails.getProductId());
                                switch (productType) {
                                    case BillingClient.ProductType.INAPP:
                                        ProductDetails.OneTimePurchaseOfferDetails details = productDetails.getOneTimePurchaseOfferDetails();
                                        jSkuDetails.put("price", details.getFormattedPrice());
                                        jSkuDetails.put("currency", details.getPriceCurrencyCode());
                                        jSkuDetails.put("priceAmountMicros", details.getPriceAmountMicros());
                                        jSkuDetails.put("title", productDetails.getTitle());
                                        break;
                                    case BillingClient.ProductType.SUBS:
                                        JSONArray jSubscriptionOffers = new JSONArray();
                                        jSkuDetails.put("subscriptionOffers", jSubscriptionOffers);
                                        List<ProductDetails.SubscriptionOfferDetails> offerDetailsList = productDetails.getSubscriptionOfferDetails();
                                        for (ProductDetails.SubscriptionOfferDetails offerDetails : offerDetailsList) {
                                            List<ProductDetails.PricingPhase> pricingPhasesList = offerDetails.getPricingPhases().getPricingPhaseList();
                                            JSONObject jOfferDetails = new JSONObject();
                                            jSubscriptionOffers.put(jOfferDetails);
                                            jOfferDetails.put("basePlanId", offerDetails.getBasePlanId());
                                            jOfferDetails.put("offerId", offerDetails.getOfferId());
                                            jOfferDetails.put("offerToken", offerDetails.getOfferToken());
                                            jOfferDetails.put("tags", new JSONArray(offerDetails.getOfferTags()).toString());
                                            JSONArray jPricingPhases = new JSONArray();
                                            jOfferDetails.put("pricingPhases", jPricingPhases);
                                            for (ProductDetails.PricingPhase phase : pricingPhasesList) {
                                                JSONObject jPricingPhase = new JSONObject();
                                                jPricingPhase.put("billingCycleCount", phase.getBillingCycleCount());
                                                jPricingPhase.put("billingPeriod", phase.getBillingPeriod());
                                                jPricingPhase.put("price", phase.getFormattedPrice());
                                                jPricingPhase.put("priceAmountMicros", phase.getPriceAmountMicros());
                                                jPricingPhase.put("currency", phase.getPriceCurrencyCode());
                                                jPricingPhase.put("recurrenceMode", phase.getRecurrenceMode());
                                                jPricingPhases.put(jPricingPhase);
                                            }
                                        }
                                        break;
                                    default:
                                        continue;
                                }
                                jSkusDetails.put(jSkuDetails);
                            }

                            int returnMap = RunnerJNILib.jCreateDsMap(null, null, null);
                            RunnerJNILib.DsMapAddDouble(returnMap, "id", ASYNC_RESPONSE_SKU_DETAILS);
                            RunnerJNILib.DsMapAddDouble(returnMap, "responseCode", responseCode);
                            RunnerJNILib.DsMapAddString(returnMap, "productType", productType);
                            RunnerJNILib.DsMapAddString(returnMap, "productDetails", jSkusDetails.toString());
                            RunnerJNILib.CreateAsynEventWithDSMap(returnMap, EVENT_OTHER_SOCIAL);
                        }
                        catch (JSONException e) {
                            sendEmptyAsyncEvent(ASYNC_RESPONSE_SKU_DETAILS, BillingClient.BillingResponseCode.ERROR);
                        }
                    }
                    else {
                        sendEmptyAsyncEvent(ASYNC_RESPONSE_SKU_DETAILS, responseCode);
                    }
                }
            });
        }
        catch (JSONException e) {
            sendEmptyAsyncEvent(ASYNC_RESPONSE_SKU_DETAILS, BillingClient.BillingResponseCode.ERROR);
        }
    }

    private static void sendEmptyAsyncEvent(int eventId, int responseCode) {
        int returnMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddDouble(returnMap, "id", eventId);
        RunnerJNILib.DsMapAddDouble(returnMap, "responseCode", responseCode);
        RunnerJNILib.CreateAsynEventWithDSMap(returnMap, EVENT_OTHER_SOCIAL);
    }

    public static void launchBillingFlow(final String productId, final String productType, final String offerToken) {
        Runnable billingFlowRunnable = new Runnable() {
            @Override
            public void run() {
                ImmutableList<QueryProductDetailsParams.Product> products = ImmutableList.of(QueryProductDetailsParams.Product.newBuilder()
                        .setProductId(productId)
                        .setProductType(productType)
                        .build());

                queryProductDetailsAsync(products, new ProductDetailsResponseListener() {
                    @Override
                    public void onProductDetailsResponse(@NonNull BillingResult billingResult, @NonNull List<ProductDetails> productDetailsList) {
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            if (productDetailsList.size() == 0) {
                                sendAsyncEventPurchaseUpdated("[" + productId + "]", BillingClient.BillingResponseCode.ERROR, "", "", "");
                                return;
                            }
                            ProductDetails productDetails = productDetailsList.get(0);
                            BillingFlowParams.ProductDetailsParams.Builder productDetailsParamsBuilder = BillingFlowParams.ProductDetailsParams.newBuilder()
                                    .setProductDetails(productDetails);

                            if (productType.equals(BillingClient.ProductType.SUBS))
                                productDetailsParamsBuilder.setOfferToken(offerToken);

                            ImmutableList<BillingFlowParams.ProductDetailsParams> productDetailsParamsList = ImmutableList.of(productDetailsParamsBuilder.build());
                            BillingFlowParams billingParams = BillingFlowParams.newBuilder()
                                    .setProductDetailsParamsList(productDetailsParamsList)
                                    .build();

                            billingClient.launchBillingFlow(RunnerActivity.CurrentActivity, billingParams);
                        }
                        else {
                            sendAsyncEventPurchaseUpdated("[" + productId + "]", responseCode, "", "", "");
                        }
                    }

                });
            }
        };

        if (billingClient.isReady())
            billingFlowRunnable.run();
        else //will try to reconnect and send purchase request again
            connectToBillingService(billingFlowRunnable);
    }

    private static ConsumeParams buildConsumeParams(String purchaseToken) {
        return ConsumeParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .build();
    }

    public static void consumeAsync(final String purchaseToken) {
        Runnable consumeRunnable = new Runnable() {
            @Override
            public void run() {
                billingClient.consumeAsync(buildConsumeParams(purchaseToken), new ConsumeResponseListener() {
                    @Override
                    public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String purchaseToken) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                            sendAsyncEventConsumeResponse(purchaseToken);
                        else
                            addUnconsumedPurchase(purchaseToken);
                    }
                });
            }
        };

        if (billingClient.isReady()) {
            consumeRunnable.run();
        }
        else
            connectToBillingService(consumeRunnable);
    }
    private static void sendAsyncEventConsumeResponse(String purchaseToken) {
        int consumeMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddDouble(consumeMap, "id", ASYNC_RESPONSE_CONSUME);
        RunnerJNILib.DsMapAddDouble(consumeMap, "responseCode", BillingClient.BillingResponseCode.OK);
        RunnerJNILib.DsMapAddString(consumeMap, "purchaseToken", purchaseToken);
        RunnerJNILib.CreateAsynEventWithDSMap(consumeMap, EVENT_OTHER_SOCIAL);
    }

    static void sendAsyncEventPurchaseUpdated(String skusStringedArray, int responseCode, String purchaseToken, String originalJson, String signature) {
        int purchaseMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddDouble(purchaseMap, "id", GooglePlayBillingWrapper.ASYNC_RESPONSE_PURCHASE_UPDATED);
        RunnerJNILib.DsMapAddDouble(purchaseMap, "responseCode", responseCode);
        RunnerJNILib.DsMapAddString(purchaseMap, "skus", skusStringedArray);
        RunnerJNILib.DsMapAddString(purchaseMap, "purchaseToken", purchaseToken);
        RunnerJNILib.DsMapAddString(purchaseMap, "originalJson", originalJson);
        RunnerJNILib.DsMapAddString(purchaseMap, "signature", signature);
        RunnerJNILib.CreateAsynEventWithDSMap(purchaseMap, EVENT_OTHER_SOCIAL);
    }

    private static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) RunnerActivity.CurrentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            if (capabilities == null)
                return false;
            return  (capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET));
        }
        else {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
        }
    }

    static boolean verifyPurchase(Purchase purchase) {
        if (publicKey == null)
            return true;
        try {
            Signature signatureInstance = Signature.getInstance("SHA1withRSA");
            signatureInstance.initVerify(publicKey);
            signatureInstance.update(purchase.getOriginalJson().getBytes());
            return signatureInstance.verify(Base64.decode(purchase.getSignature(), Base64.DEFAULT));
        }
        catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
            return false;
        }
    }

    static void acknowledgePurchase(Purchase purchase) {
        AcknowledgePurchaseParams acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(purchase.getPurchaseToken())
                .build();
        GooglePlayBillingWrapper.billingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
            @Override
            public void onAcknowledgePurchaseResponse(@NonNull BillingResult billingResult) {}
        });
    }
}

class PurchasesUpdatedListenerImpl implements PurchasesUpdatedListener {
    @Override
    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {
        int responseCode = billingResult.getResponseCode();
        if (responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            for (Purchase purchase : purchases) {
                if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
                    if (!GooglePlayBillingWrapper.verifyPurchase(purchase))
                        return;
                    if (!purchase.isAcknowledged())
                        GooglePlayBillingWrapper.acknowledgePurchase(purchase);
                    JSONArray jSKUs = new JSONArray(purchase.getProducts());
                    GooglePlayBillingWrapper.sendAsyncEventPurchaseUpdated(jSKUs.toString(), responseCode, purchase.getPurchaseToken(), purchase.getOriginalJson(), purchase.getSignature());
                }
            }
        }
        else {
            GooglePlayBillingWrapper.sendAsyncEventPurchaseUpdated("[]", responseCode, "", "", "");
        }
    }
}
